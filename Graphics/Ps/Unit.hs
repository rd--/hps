-- | Unit definitions and conversions.
module Graphics.Ps.Unit where

{- | Convert PS points to millimeters.
A pt is 1/72 inches.  An inch is 25.4 mm.

>>> pt_to_mm 1 == 25.4 / 72
True

>>> pt_to_mm 72
25.4

>>> map (round . (* 100) . pt_to_mm) [8,9,11,18]
[282,318,388,635]
-}
pt_to_mm :: Floating a => a -> a
pt_to_mm = (/ (72 / 25.4))

{- | Convert millimeters to PS points.

>>> mm_to_pt 25.4
72.0
-}
mm_to_pt :: Floating a => a -> a
mm_to_pt = (* (72 / 25.4))

-- | Type specialised 'fromIntegral'.
int_to_double :: Int -> Double
int_to_double = fromIntegral

-- | 'Int' variant of 'mm_pt'.
mm_pt_int :: Int -> Int
mm_pt_int = floor . mm_to_pt . int_to_double

{--
-- | Postscript makes a point exactly 1/72 of an inch.
inch :: (Num a) => a -> a
inch n = 72 * n

-- | 1 cm = 0.393700787 inches
cm :: Floating a => a -> a
cm n = 28.346456664 * n

twopi :: Floating a => a
twopi = 2 * pi

halfpi :: Floating a => a
halfpi = pi / 2
--}
