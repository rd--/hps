-- | Gs = graphics-state
module Graphics.Ps.Gs where

-- | Line cap enumeration.
data LineCap
  = ButtCap
  | RoundCap
  | ProjectingSquareCap
  deriving (Eq, Show, Enum)

-- | Line width (real).
type LineWidth = Double

-- | Line join enumeration.
data LineJoin
  = MiterJoin
  | RoundJoin
  | BevelJoin
  deriving (Eq, Show, Enum)

-- | Colour model.  Postscript doesn't support alpha, but store it for PDF rendering etc.
data Color = RGBA Double Double Double Double
  deriving (Eq, Show)

-- | Graphics state.
data Gs = Gs
  { gs_color :: Color
  , gs_line_width :: LineWidth
  , gs_line_cap :: LineCap
  , gs_line_join :: LineJoin
  , gs_dash :: ([Int], Int)
  , gs_miter_limit :: Double
  }
  deriving (Eq, Show)

-- | Default 'Gs' of indicated 'Color'.
defaultGs :: Color -> Gs
defaultGs c = Gs c 1.0 ButtCap MiterJoin ([], 0) 10.0

-- | Default 'Gs' of indicated shade of grey.
greyGs :: Double -> Gs
greyGs g = defaultGs (RGBA g g g 1)

{--
blackGs :: Gs
blackGs = greyGs 0
--}
