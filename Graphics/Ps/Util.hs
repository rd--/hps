module Graphics.Ps.Util where

import Control.Monad {- base -}
import System.FilePath {- filepath -}
import System.Process {- process -}

-- | (cmd,arg)
type Cmd = (String, [String])

-- | 'rawSystem' of 'Cmd'.
run_cmd :: Cmd -> IO ()
run_cmd = void . uncurry rawSystem

{- | ps2pdf command to convert EPS file to PDF, replacing extension.
  <https://www.ghostscript.com/>
-}
eps_to_pdf_cmd :: FilePath -> Cmd
eps_to_pdf_cmd fn = ("ps2pdf", ["-dEPSCrop", fn, replaceExtension fn ".pdf"])

{- | Run ps2pdf to translate an EPS file to a PDF file.

> eps_to_pdf "/tmp/t.0.eps"
-}
eps_to_pdf :: FilePath -> IO ()
eps_to_pdf = run_cmd . eps_to_pdf_cmd

-- | <https://github.com/dawbarton/pdf2svg>
pdf_to_svg :: FilePath -> IO ()
pdf_to_svg fn = run_cmd ("pdf2svg", [fn, replaceExtension fn ".svg"])

eps_to_svg :: FilePath -> IO ()
eps_to_svg fn = eps_to_pdf fn >> pdf_to_svg fn
