-- | Path = geometry (without GS)
module Graphics.Ps.Path where

import Data.List {- base -}
import Data.Monoid {- base -}
import Data.Semigroup {- base -}

import Data.Cg.Minus.Types {- hcg-minus -}

import Graphics.Ps.Cg

-- | Font data type.
data Font = Font
  { fontName :: String
  , fontSize :: R
  }
  deriving (Eq, Show)

-- | Character data type.
type Glyph = Char

-- | Path data type, in cartesian space.
data Path
  = MoveTo (Pt R)
  | LineTo (Pt R)
  | CurveTo (Pt R) (Pt R) (Pt R)
  | ClosePath (Pt R)
  | Text Font [Glyph]
  | PTransform (Matrix R) Path
  | Join Path Path
  deriving (Eq, Show)

instance Data.Semigroup.Semigroup Path where
  (<>) = Join

instance Data.Monoid.Monoid Path where
  mempty = MoveTo (Pt 0 0)
  mconcat [] = mempty
  mconcat paths = combine paths

-- | Left fold of 'Join'.
combine :: [Path] -> Path
combine = foldl1 Join

-- | Line segments though list of 'Pt'.
line :: [Pt R] -> Path
line x =
  case x of
    [] -> error "line: illegal data"
    (p : ps) -> combine (MoveTo p : map LineTo ps)

-- | Variant of 'line' connecting the last 'Pt' to the first.
polygon :: [Pt R] -> Path
polygon x =
  case x of
    [] -> error "polygon: illegal data"
    (p : ps) -> line (p : ps) <> ClosePath p

{- | Rectangle with lower left at 'Pt' and of specified width and
height.  Polygon is ordered anticlockwise from lower left.
-}
rectangle :: Pt R -> R -> R -> Path
rectangle (Pt x y) w h =
  let ll = Pt x y
      lr = Pt (x + w) y
      ur = Pt (x + w) (y + h)
      ul = Pt x (y + h)
  in polygon [ll, lr, ur, ul]

-- | An arc segment, as starting point and values for the curveto operator.
type Arc_Seg n = (Pt n, Pt n, Pt n, Pt n)

-- | An arc, given as either one or two segments.
data Arc n
  = Arc1 (Arc_Seg n)
  | Arc2 (Arc_Seg n) (Arc_Seg n)

-- | Arc segment, (x,y) = center,r = radius,a = start angle,b = end angle.
arcp :: Pt R -> R -> R -> R -> Arc_Seg R
arcp (Pt x y) r a b =
  let ca = cos a
      sa = sin a
      cb = cos b
      sb = sin b
      bcp = 4 / 3 * (1 - cos ((b - a) / 2)) / sin ((b - a) / 2)
      p0 = Pt (x + r * ca) (y + r * sa)
      p1 = Pt (x + r * (ca - bcp * sa)) (y + r * (sa + bcp * ca))
      p2 = Pt (x + r * (cb + bcp * sb)) (y + r * (sb - bcp * cb))
      p3 = Pt (x + r * cb) (y + r * sb)
  in (p0, p1, p2, p3)

{- | Arc, c = center,r = radius,a = start angle,b = end angle

If the arc angle is greater than pi the arc must be drawn in two segments.
-}
arca :: Pt R -> R -> R -> R -> Arc R
arca c r a b =
  let d = abs (b - a)
      b' = b - (d / 2)
  in if d > pi
      then Arc2 (arcp c r a b') (arcp c r b' b)
      else Arc1 (arcp c r a b)

-- | 'Path' of 'Arc'.
arc_to_path :: Arc R -> Path
arc_to_path a =
  case a of
    Arc1 (p0, p1, p2, p3) -> MoveTo p0 <> CurveTo p1 p2 p3
    Arc2 (p0, p1, p2, p3) (_, p5, p6, p7) -> MoveTo p0 <> CurveTo p1 p2 p3 <> CurveTo p5 p6 p7

-- | Variant of 'arca' allowing b to be less than a.
arca_udir :: Pt R -> R -> R -> R -> Arc R
arca_udir c r a b =
  let b' = if b < a then b + 2 * pi else b
  in arca c r a b'

-- | 'arca_udir' with a and b reversed.
arcNegative_udir :: Pt R -> R -> R -> R -> Arc R
arcNegative_udir c r a b =
  let b' = if b > a then b - 2 * pi else b
  in arca c r b' a

-- | Arc given by a central point,a radius,and start and end angles.
arc :: Pt R -> R -> R -> R -> Path
arc c r a = arc_to_path . arca_udir c r a

-- | Negative arc.
arcNegative :: Pt R -> R -> R -> R -> Path
arcNegative c r a = arc_to_path . arcNegative_udir c r a

type Annular n = (Pt n, Pt n, Arc n, Pt n, Arc n)

{- | (x,y) = center,ir = inner radius,xr = outer radius,sa = start
angle,a = angle,ea = end angle
-}
annular_f :: Pt R -> R -> R -> R -> R -> Annular R
annular_f (Pt x y) ir xr sa a =
  let ea = sa + a
      x2 = x + ir * cos sa -- ll
      y2 = y + ir * sin sa
      x3 = x + xr * cos sa -- ul
      y3 = y + xr * sin sa
      x4 = x + ir * cos ea -- lr
      y4 = y + ir * sin ea
  in ( Pt x2 y2
     , Pt x3 y3
     , arca_udir (Pt x y) xr sa ea
     , Pt x4 y4
     , arcNegative_udir (Pt x y) ir ea sa
     )

-- | Annular segment.
annular :: Pt R -> R -> R -> R -> R -> Path
annular c ir xr sa a =
  let (p1, p2, a1, p3, a2) = annular_f c ir xr sa a
  in combine [MoveTo p1, LineTo p2, arc_to_path a1, LineTo p3, arc_to_path a2]

-- | Render each (p1,p2) as a distinct line.
renderLines :: [Ln R] -> Path
renderLines =
  let f pth (Ln p1 p2) = pth <> MoveTo p1 <> LineTo p2
  in foldl f (MoveTo (Pt 0 0))

-- | Collapse line sequences into a single line.
renderLines_jn :: [Ln R] -> Path
renderLines_jn =
  let g p (Ln a b) =
        if p == a
          then (b, Right b)
          else (b, Left (Ln a b))
      f path e = case e of
        Left (Ln p1 p2) -> path <> MoveTo p1 <> LineTo p2
        Right p2 -> path <> LineTo p2
  in foldl f (MoveTo (Pt 0 0)) . snd . mapAccumL g (Pt 0 0)

{-

flatten_f :: Matrix R -> Path -> Path
flatten_f m path =
    case path of
      MoveTo p -> MoveTo (CG.pt_transform m p)
      LineTo p -> LineTo (CG.pt_transform m p)
      ClosePath p -> ClosePath (CG.pt_transform m p)
      PTransform m' p -> flatten_f (m' * m) p
      Join a b -> Join (flatten_f m a) (flatten_f m b)
      CurveTo p q r -> let f = CG.pt_transform m
                       in CurveTo (f p) (f q) (f r)
      Text _ _ -> error "cannot flatten text"

-- | Apply any transformations at path.  The resulting path will not
--   have any 'PTransform' nodes.
flatten :: Path -> Path
flatten = flatten_f CG.mx_identity

curve :: Pt -> Pt -> Pt -> Pt -> Path
curve p c1 c2 q = MoveTo p <> CurveTo c1 c2 q

-- | Polar variant.
pMoveTo :: Pt -> Path
pMoveTo p = MoveTo (polarToRectangular p)

-- | Polar variant.
pLineTo :: Pt -> Path
pLineTo p = LineTo (polarToRectangular p)

-- | Apply a funtion to leaf nodes.
p_apply :: (Path -> Path) -> Path -> Path
p_apply f (Join p q) = Join (f p) (f q)
p_apply f (PTransform m p) = PTransform m (f p)
p_apply f p = f p

--}
