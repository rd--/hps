-- | Postscript generator.
module Graphics.Ps.Ps where

import Data.List {- base -}
import Data.Semigroup {- base -}

import Data.Cg.Minus.Types {- hcg-minus -}

import Graphics.Ps.Cg
import qualified Graphics.Ps.Gs as Gs
import qualified Graphics.Ps.Image as Image
import qualified Graphics.Ps.Paper as Paper
import qualified Graphics.Ps.Path as Path

data Ps
  = Name String
  | LName String
  | Op String
  | Comment String
  | Int Int
  | Double R
  | String String
  | Transform (Matrix R)
  | Array [Ps]
  | Proc [Ps]
  | Dict [(Ps, Ps)]
  | Seq [Ps]

instance Data.Semigroup.Semigroup Ps where
  p <> q = Seq [p, q]

dscPs :: String -> [String] -> Ps
dscPs k v =
  case v of
    [] -> Comment ('%' : k)
    _ -> Comment (concat ("%" : k : ": " : intersperse " " v))

headerPs :: Ps
headerPs = Comment "!Ps-Adobe-3.0"

headerEPs :: Ps
headerEPs = Comment "!Ps-Adobe-3.0 EPsF-3.0"

titlePs :: String -> Ps
titlePs t = dscPs "Title" [t]

creatorPs :: String -> Ps
creatorPs t = dscPs "Creator" [t]

languageLevelPs :: Int -> Ps
languageLevelPs n = dscPs "LanguageLevel" [show n]

pagesPs :: Int -> Ps
pagesPs n = dscPs "Pages" [show n]

bboxPs :: Paper.BBox -> Ps
bboxPs b =
  case b of
    Paper.BBox i j k l -> dscPs "BoundingBox" [show i, show j, show k, show l]
    Paper.HRBBox i j k l i' j' k' l' ->
      Seq
        [ dscPs "BoundingBox" [show i, show j, show k, show l]
        , dscPs "HiResBoundingBox" [show i', show j', show k', show l']
        ]

endCommentsPs :: Ps
endCommentsPs = dscPs "EndComments" []

pagePs :: (Show a) => String -> a -> Ps
pagePs t n = dscPs "Page" [t, show n]

trailerPs :: Ps
trailerPs = dscPs "Trailer" []

eofPs :: Ps
eofPs = dscPs "EOF" []

documentMediaPs :: String -> Int -> Int -> Ps
documentMediaPs tag w h = dscPs "DocumentMedia" [tag, show w, show h, "0", "()", "()"]

aliasPs :: String -> String -> Ps
aliasPs o a = Seq [LName a, Proc [Name o], Op "def"]

pdfCompat :: [(String, String)]
pdfCompat =
  [ ("gsave", "q")
  , ("grestore", "Q")
  , ("stroke", "S")
  , ("fill", "f")
  , ("setrgbcolor", "RG")
  , ("setlinewidth", "w")
  , ("setlinecap", "J")
  , ("setlinejoin", "j")
  , ("setdash", "d")
  , ("setmiterlimit", "M")
  , ("moveto", "m")
  , ("lineto", "l")
  , ("curveto", "c")
  , ("closepath", "h")
  , ("concat", "cm")
  , ("show", "Tj")
  , ("selectfont", "Tf")
  , ("clip", "W")
  ]

prologPs :: Ps
prologPs =
  let f (a, b) = aliasPs a b
  in Seq (map f pdfCompat)

strokePs :: Ps
strokePs = Op "S"

fillPs :: Ps
fillPs = Op "f"

falsePs :: Ps
falsePs = Name "false"

savePs :: Ps
savePs = Op "q"

restorePs :: Ps
restorePs = Op "Q"

showPagePs :: Ps
showPagePs = Op "showpage"

rgbaPs :: Gs.Color -> Ps
rgbaPs (Gs.RGBA r g b _) = Seq [Double r, Double g, Double b, Op "RG"]

lineWidthPs :: R -> Ps
lineWidthPs w = Seq [Double w, Op "w"]

lineCapPs :: (Enum a) => a -> Ps
lineCapPs c = Seq [Int (fromEnum c), Op "j"]

lineJoinPs :: (Enum a) => a -> Ps
lineJoinPs j = Seq [Int (fromEnum j), Op "J"]

dashPs :: [Int] -> Int -> Ps
dashPs d o = Seq [Array (map Int d), Int o, Op "d"]

miterLimitPs :: R -> Ps
miterLimitPs m = Seq [Double m, Op "M"]

moveToPs :: Pt R -> Ps
moveToPs (Pt x y) = Seq [Double x, Double y, Op "m"]

lineToPs :: Pt R -> Ps
lineToPs (Pt x y) = Seq [Double x, Double y, Op "l"]

transformPs :: Matrix R -> Ps
transformPs m = Seq [Transform m, Op "cm"]

curveToPs :: Pt R -> Pt R -> Pt R -> Ps
curveToPs (Pt x1 y1) (Pt x2 y2) (Pt x3 y3) = Seq (map Double [x1, y1, x2, y2, x3, y3] ++ [Op "c"])

closePathPs :: Pt R -> Ps
closePathPs (Pt x y) = Seq [Double x, Double y, Op "h"]

selectFontPs :: Path.Font -> Ps
selectFontPs (Path.Font f n) = Seq [LName f, Double n, Op "Tf"]

charPathPs :: String -> Ps
charPathPs g = Seq [String g, falsePs, Op "charpath"]

gsPs :: Gs.Gs -> Ps
gsPs (Gs.Gs c w k j (d, o) m) =
  Seq
    [ rgbaPs c
    , lineWidthPs w
    , lineCapPs k
    , lineJoinPs j
    , dashPs d o
    , miterLimitPs m
    ]

pathPs :: Path.Path -> Ps
pathPs path =
  case path of
    Path.MoveTo p -> moveToPs p
    Path.LineTo p -> lineToPs p
    Path.Text f g -> Seq [selectFontPs f, charPathPs g]
    Path.CurveTo c1 c2 p -> curveToPs c1 c2 p
    Path.ClosePath p -> closePathPs p
    Path.PTransform m p -> Seq [transformPs m, pathPs p]
    Path.Join a b -> Seq [pathPs a, pathPs b]

imagePs :: Image.Image -> Ps
imagePs img =
  case img of
    Image.Empty -> Comment "Empty"
    Image.Stroke g p -> Seq [pathPs p, gsPs g, strokePs]
    Image.Fill g p -> Seq [pathPs p, gsPs g, fillPs]
    Image.ITransform m i -> Seq [transformPs m, imagePs i]
    Image.Over a b ->
      Seq
        [ savePs
        , imagePs b
        , restorePs
        , imagePs a
        ]

ps_bracket :: String -> String -> [Ps] -> String
ps_bracket o c p =
  let h a = a ++ " "
  in o ++ concatMap ps_to_string p ++ h c

ps_escape :: String -> String
ps_escape = concatMap (\c -> if elem c "()" then ['\\', c] else [c])

ps_to_string :: Ps -> String
ps_to_string x =
  case x of
    Name n -> n ++ " "
    LName n -> "/" ++ n ++ " "
    Op o -> o ++ "\n"
    Comment o -> "%" ++ o ++ "\n"
    Int n -> (show n) ++ " "
    Double n -> (show n) ++ " "
    String s -> "(" ++ s ++ ") "
    Array a -> ps_bracket "[" "]" a
    Proc p -> ps_bracket "{" "}" p
    Transform (Matrix a1 a2 b1 b2 c1 c2) -> ps_to_string (Array (map Double [a1, a2, b1, b2, c1, c2]))
    Dict d -> let g = concatMap (\(a, b) -> [a, b]) in ps_bracket "<<" ">>" (g d)
    Seq a -> concatMap ps_to_string a

to_page_seq :: (Image.Image, Int) -> Ps
to_page_seq (p, n) = Seq [pagePs "Graphics.Ps" n, imagePs p, showPagePs]

-- | Write a postscript file.  The list of images are written one per page.
ps :: FilePath -> Paper.Paper -> [Image.Image] -> IO ()
ps f d p = writeFile f (stringFromPs f d p)

image_to_ps :: String -> (Int, Int) -> [Image.Image] -> Ps
image_to_ps t (width, height) p =
  Seq
    [ headerPs
    , titlePs t
    , creatorPs "Graphics.Ps"
    , languageLevelPs 2
    , pagesPs (length p)
    , documentMediaPs "Default" width height
    , endCommentsPs
    , prologPs
    , Seq (map to_page_seq (zip p [1 ..]))
    , trailerPs
    , eofPs
    ]

-- | Variant with page (paper) size in points.
stringFromPs_pt :: String -> (Int, Int) -> [Image.Image] -> String
stringFromPs_pt t dm p = ps_to_string (image_to_ps t dm p)

{- | Generate postscript data given /title/, page size, and a
set of page 'Image.Images'.
-}
stringFromPs :: String -> Paper.Paper -> [Image.Image] -> String
stringFromPs t p = stringFromPs_pt t (Paper.paper_size_pt p)

-- | Image to EPs variant of Ps.
image_to_eps :: FilePath -> Paper.BBox -> Image.Image -> Ps
image_to_eps fn d p =
  Seq
    [ headerEPs
    , titlePs ("Graphics.Ps: " ++ fn)
    , creatorPs "Graphics.Ps"
    , languageLevelPs 2
    , bboxPs d
    , endCommentsPs
    , prologPs
    , imagePs p
    ]

{- | Write an encapsulated postscript file.  The 'Paper.BBox' is in
points.  A single image is written.
-}
eps :: FilePath -> Paper.BBox -> Image.Image -> IO ()
eps fn bx i = writeFile fn (ps_to_string (image_to_eps fn bx i))

{--

data Orientation = Portrait
                 | Landscape
                   deriving (Show)

orientation :: Orientation -> Ps
orientation o = dsc "Orientation" [show o]

creationDate :: String -> Ps
creationDate t = dsc "CreationDate" [t]

setFont :: Ps
setFont = Op "setfont"

show :: String -> Ps
show g = [String g, Op "h"]

translate :: Double -> Double -> Ps
translate x y = [Double x, Double y, Op "translate"]

scale :: Double -> Double -> Ps
scale x y = [Double x, Double y, Op "scale"]

rotate :: Double -> Ps
rotate a = [Double a, Op "rotate"]

findFont :: Font -> Ps
findFont (Font f _) = [LName f, Op "findfont"]

scaleFont :: Font -> Ps
scaleFont (Font _ n) = [Double n, Op "scalefont"]

--}
