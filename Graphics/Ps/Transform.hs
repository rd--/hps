-- | Class and associated functions for 'Matrix' transformations.
module Graphics.Ps.Transform where

import Data.Cg.Minus.Types {- hcg-minus -}

import qualified Graphics.Ps.Image as I
import qualified Graphics.Ps.Path as P

-- | Values that can be transformed in relation to a 'Matrix'.
class Transformable t where
  mtransform :: Matrix Double -> t -> t

-- | Translation in /x/ and /y/.
translate :: Transformable t => Double -> Double -> t -> t
translate x y = mtransform (Matrix 1 0 0 1 x y)

-- | Scaling in /x/ and /y/.
scale :: Transformable t => Double -> Double -> t -> t
scale x y = mtransform (Matrix x 0 0 y 0 0)

-- | Rotation, in radians.
rotate :: Transformable t => Double -> t -> t
rotate a = mtransform (Matrix (cos a) (sin a) (negate (sin a)) (cos a) 0 0)

instance Transformable I.Image where
  mtransform = I.ITransform

instance Transformable P.Path where
  mtransform = P.PTransform

-- instance Transformable (Pt Double) where
--    mtransform = pt_transform

{--
import Graphics.Ps.Pt

-- | Polar variant.
pTranslate :: (Transform a) => Double -> Double -> a -> a
pTranslate r t = translate x y
    where (Pt x y) = polarToRectangular (Pt r t)

--}
