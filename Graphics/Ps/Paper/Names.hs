{- | Names of paper sizes.
For ISO sizes see <http://www.cl.cam.ac.uk/~mgk25/iso-paper.html>.
-}
module Graphics.Ps.Paper.Names where

import Graphics.Ps.Paper

{- | ISO size downscaling, ie. from @A0@ to @A1@.

>>> iso_down_scale a4 == a5
True
-}
iso_down_scale :: Paper -> Paper
iso_down_scale (Paper u w h) = Paper u (h `divRound` 2) w

-- | Jis size downscaling, truncates instead of rounding.
jis_down_scale :: Paper -> Paper
jis_down_scale (Paper u w h) = Paper u (h `div` 2) w

{- | Iso A sizes in millimeters.

>>> a4 == Paper Mm 210 297
True

>>> paper_size_pt a3
(841,1190)
-}
a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10 :: Paper
a0 = Paper Mm 841 1189
a1 = iso_down_scale a0
a2 = iso_down_scale a1
a3 = iso_down_scale a2
a4 = iso_down_scale a3
a5 = iso_down_scale a4
a6 = iso_down_scale a5
a7 = iso_down_scale a6
a8 = iso_down_scale a7
a9 = iso_down_scale a8
a10 = iso_down_scale a9

{- | Iso B sizes in millimeters.

>>> b4 == Paper Mm 250 354
True

>>> inset_margin b4 a3
(Mm,23,33)
-}
b0, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10 :: Paper
b0 = Paper Mm 1000 1414
b1 = iso_down_scale b0
b2 = iso_down_scale b1
b3 = iso_down_scale b2
b4 = iso_down_scale b3
b5 = iso_down_scale b4
b6 = iso_down_scale b5
b7 = iso_down_scale b6
b8 = iso_down_scale b7
b9 = iso_down_scale b8
b10 = iso_down_scale b9

{- | Jis B sizes

>>> jis_b4 == Paper Mm 257 364
True

>>> (proportion b0,proportion jis_b0)
(1.414,1.4135922330097088)

>>> inset_margin jis_b4 a3
(Mm,20,28)
-}
jis_b0, jis_b1, jis_b2, jis_b3, jis_b4, jis_b5, jis_b6, jis_b7, jis_b8, jis_b9, jis_b10 :: Paper
jis_b0 = Paper Mm 1030 1456
jis_b1 = jis_down_scale jis_b0
jis_b2 = jis_down_scale jis_b1
jis_b3 = jis_down_scale jis_b2
jis_b4 = jis_down_scale jis_b3
jis_b5 = jis_down_scale jis_b4
jis_b6 = jis_down_scale jis_b5
jis_b7 = jis_down_scale jis_b6
jis_b8 = jis_down_scale jis_b7
jis_b9 = jis_down_scale jis_b8
jis_b10 = jis_down_scale jis_b9

{- | ISO C sizes in millimeters.

>>> c4 == Paper Mm 229 324
True
-}
c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10 :: Paper
c0 = Paper Mm 917 1297
c1 = iso_down_scale c0
c2 = iso_down_scale c1
c3 = iso_down_scale c2
c4 = iso_down_scale c3
c5 = iso_down_scale c4
c6 = iso_down_scale c5
c7 = iso_down_scale c6
c8 = iso_down_scale c7
c9 = iso_down_scale c8
c10 = iso_down_scale c9

{- | US papers sizes in millimeters.

>>> map proportion [usLetter,usLegal,usExecutive,usLedger]
[1.2916666666666667,1.6481481481481481,1.3368421052631578,1.5483870967741935]
-}
usLetter, usLegal, usExecutive, usLedger :: Paper
usLetter = Paper Mm 216 279
usLegal = Paper Mm 216 356
usExecutive = Paper Mm 190 254
usLedger = Paper Mm 279 432

{- | American National Standard ANSI/ASME Y14.1 (technical drawing paper sizes).

>>> map proportion [ansi_y14_A,ansi_y14_B]
[1.2916666666666667,1.5483870967741935]
-}
ansi_y14_A, ansi_y14_B, ansi_y14_C, ansi_y14_D, ansi_y14_E :: Paper
ansi_y14_A = Paper Mm 216 279
ansi_y14_B = Paper Mm 279 432
ansi_y14_C = Paper Mm 432 559
ansi_y14_D = Paper Mm 559 864
ansi_y14_E = Paper Mm 864 1118

{- | Newspaper sizes in millimeters.
See <http://www.papersizes.org/newspaper-sizes.htm>.

>>> map proportion [broadsheet,berliner,tabloid]
[1.25,1.492063492063492,1.5357142857142858]
-}
broadsheet, berliner, tabloid :: Paper
broadsheet = Paper Mm 600 750
berliner = Paper Mm 315 470
tabloid = Paper Mm 280 430

paper_gen_table :: (Paper -> Paper) -> Char -> Paper -> [(String, Int, Int)]
paper_gen_table scale_f nm p0 =
  let p_seq = take 11 (iterate scale_f p0)
      f k (Paper _ w h) = (nm : show k, w, h)
  in zipWith f [0 :: Int .. 10] p_seq

{-
> iso_paper_gen_table 'A' a0
> iso_paper_gen_table 'B' b0
> iso_paper_gen_table 'C' c0
-}
iso_paper_gen_table :: Char -> Paper -> [(String, Int, Int)]
iso_paper_gen_table = paper_gen_table iso_down_scale

{-
> jis_paper_gen_table 'B' jis_b0
-}
jis_paper_gen_table :: Char -> Paper -> [(String, Int, Int)]
jis_paper_gen_table = paper_gen_table jis_down_scale
