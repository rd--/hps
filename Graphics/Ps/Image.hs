-- | Image = Path + Gs
module Graphics.Ps.Image where

import Data.Monoid {- base -}
import Data.Semigroup {- base -}

import Data.Cg.Minus.Types {- hcg-minus -}

import Graphics.Ps.Gs
import Graphics.Ps.Path

-- | An image is a rendering of a graph of 'Path's.
data Image
  = Stroke Gs Path
  | Fill Gs Path
  | ITransform (Matrix Double) Image
  | -- | Place one image over another
    Over Image Image
  | Empty
  deriving (Eq, Show)

instance Data.Semigroup.Semigroup Image where
  (<>) = Over

instance Data.Monoid.Monoid Image where
  mempty = Empty
  mconcat = foldr Over Empty

{-
-- | Layer one 'Image' over another.
over :: Image -> Image -> Image
over = Over

-- | Apply a function to leaf nodes.
i_apply :: (Image -> Image) -> Image -> Image
i_apply f (ITransform m i) = ITransform m (i_apply f i)
i_apply f (Over i j) = Over (i_apply f i) (i_apply f j)
i_apply f i = f i

-- | Apply a path function to leaf nodes.
i_p_apply :: (P.Path -> P.Path) -> Image -> Image
i_p_apply f (Stroke g p) = Stroke g (P.p_apply f p)
i_p_apply f (Fill g p) = Fill g (P.p_apply f p)
i_p_apply f (ITransform m i) = ITransform m (i_p_apply f i)
i_p_apply f (Over i j) = Over (i_p_apply f i) (i_p_apply f j)
i_p_apply _ Empty = Empty
-}
