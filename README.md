hps
---

[haskell](http://haskell.org)
postscript

partial and trivial rendering of the post script drawing model.

- "PostScript Language: Reference" (1985-1999)
  [PDF](http://paulbourke.net/dataformats/postscript/psref.pdf)
- "PostScript Language: Tutorial and Cookbook" (1985)
  [PDF](https://www-cdf.fnal.gov/offline/PostScript/BLUEBOOK.PDF)

© [rohan drape](http://rohandrape.net/) and others,
2006-2025,
[gpl](http://gnu.org/copyleft/).
with contributions by:

- declan murphy
- henning thielemann

see the [darcs](http://darcs.net/) [history](?t=hps&q=history) for details

* * *

```
$ make doctest
Examples: 25  Tried: 25  Errors: 0  Failures: 0
$
```
