module Main where

import Data.List.Split {- split -}
import Data.Maybe {- base -}
import System.Directory {- directory -}
import System.Environment {- base -}
import System.FilePath {- filepath -}
import System.Random {- random -}

import Data.Cg.Minus.Types {- hcg-minus -}

import Graphics.Ps {- hps -}
import qualified Graphics.Ps.Paper.Names as Paper
import qualified Graphics.Ps.Path.Graphs as Graphs

-- | Group a list into a list of /n/ element lists.
clump :: Int -> [a] -> [[a]]
clump = chunksOf

-- | Given seed /s/ generate a list of /n/ random numbers in the range [/l,r/].
randn :: Int -> Int -> Double -> Double -> [Double]
randn s n l r =
  let f i = i * (r - l) + l
  in (map f . take n . randoms . mkStdGen) s

-- | Grey fill operator.
gfill :: Double -> Path -> Image
gfill g = Fill (greyGs g)

-- | Grey stroke operator.
gstroke :: Double -> Path -> Image
gstroke g = Stroke (greyGs g)

-- | Thin grey stroke operator.
gstroke' :: Double -> Path -> Image
gstroke' g = Stroke (Gs (RGBA g g g 1.0) 0.01 RoundCap RoundJoin ([], 0) 10.0)

-- | Bold blue stroke operator.
bstroke :: Path -> Image
bstroke = Stroke (Gs (RGBA 0.0 0.0 1.0 1.0) 16.0 RoundCap RoundJoin ([], 0) 10.0)

-- | Crappy normalizer, ought to do bounds checks and take paper size.
normalize :: Path -> Path
normalize p =
  let s = 250
      z = 250
  in (translate s s . scale z z) p

{- | Text is a path constructor.  The first argument is a Font, the
  second a list of Glyphs.  Ordinarly a Text path is filled, however
  it can be stroked as below.
-}
simpleText :: Image
simpleText =
  let t = Text (Font "Times" 72) "SimpleText"
  in gstroke 0.25 (MoveTo (Pt 100 400) `Join` t)

rectangle_ :: [Double] -> Image
rectangle_ [g, x, y, z] =
  let x' = x * 500
      y' = y * 500
      z' = z * 64
      shift = translate x' y' . scale z' z'
  in gfill g (shift (rectangle (Pt 0 0) 1 1))
rectangle_ _ = error "illegal rectangle_"

-- | A random scattering of filled grey rectangles.
rectangles :: Image
rectangles = (foldl1 Over . map rectangle_ . clump 4) (randn 1 240 0 1)

-- Angles in Hps are in radians and counter-clockwise.

-- A Path must begin with a MoveTo operator.

-- | 'arc' makes a path composed of bezier curves.
semiarc :: [Double] -> Image
semiarc [g, x, y, z] =
  let x' = x * 500
      y' = y * 500
      z' = z * 64
      shift = translate x' y' . scale z' z'
  in (gstroke' g . shift . mkValid) (arc (Pt 0 0) 1 0 (1.5 * pi))
semiarc _ = error "illegal semiarc"

-- | A random scattering of stroked arcs.
semiarcs :: Image
semiarcs = (foldl1 Over . map semiarc . clump 4) (randn 1 120 0 1)

{- | 'annular' makes a path composed of arcs and lines.  g = grey, ir =
inner radius, xr = outer radius, sa = start angle, a = angle.
-}
semiann :: (Double -> Path -> Image) -> [Double] -> Image
semiann f [g, ir, xr, sa, a] =
  let x = 250
      y = 250
      z = 250
      sa' = sa * 2.0 * pi
      a' = a * pi
      ir' = min ir xr
      xr' = max ir xr
      shift = translate x y . scale z z
  in f g $ shift $ mkValid $ annular (Pt 0 0) ir' xr' sa' a'
semiann _ _ = error "illegal semiann"

-- | A random set of annular sections.
semiannsS :: Image
semiannsS = foldl1 Over $ map (semiann gstroke') $ clump 5 $ randn 1 50 0 1

semiannsF :: Image
semiannsF = foldl1 Over $ map (semiann gfill) $ clump 5 $ randn 1 50 0 1

{- | flatten| applies all tranformations on a path generating a new
 path.
-}
curve_ex :: Image
curve_ex =
  let p0 = Pt 0.1 0.5
      p1 = Pt 0.4 0.9
      p2 = Pt 0.6 0.1
      p3 = Pt 0.9 0.5
      c = MoveTo p0 `Join` CurveTo p1 p2 p3
      l = MoveTo p0 `Join` LineTo p1 `Join` MoveTo p2 `Join` LineTo p3
      s n = gstroke' n . normalize
  in s 0 c `Over` s 0.5 l

startPt_ :: Path -> Pt Double
startPt_ = fromJust . startPt

endPt_ :: Path -> Pt Double
endPt_ = fromJust . endPt

-- arcd_ex :: Image
arcd_ex :: (Pt Double -> Double -> Double -> Double -> Path) -> Image
arcd_ex arcd =
  let c = Pt 0.5 0.5
      r = 0.4
      degrees_to_radians = (* pi) . (/ 180)
      a0 = degrees_to_radians 45
      a1 = degrees_to_radians 180
      e0 = mkValid $ arcd c r a0 a1
      e1 = mkValid $ arc c 0.05 0 (2 * pi)
      e2 = MoveTo c `Join` LineTo (startPt_ e0) `Join` MoveTo c `Join` LineTo (endPt_ e0)
      s n = gstroke' n . normalize
      f n = gfill n . normalize
  in s 0 e0 `Over` f 0.9 e1 `Over` s 0.5 e2

path_ex' :: Path
path_ex' =
  MoveTo (Pt 0.5 0.1)
    `Join` LineTo (Pt 0.9 0.9)
    `Join` LineTo (Pt 0.5 0.9)
    `Join` CurveTo (Pt 0.2 0.9) (Pt 0.2 0.5) (Pt 0.5 0.5)

hps :: [Image]
hps =
  let -- arrows = G.fractalArrow 200 9
      arc_ex = arcd_ex arc
      arcNegative_ex = arcd_ex arcNegative
      path_ex = s 0 path_ex'
      -- approx_ex n = s 0 (approx n path_ex')
      fs_ex =
        let e = close path_ex'
        in s 0 e `Over` f 0.9 e
      s n = gstroke' n . normalize
      f n = gfill n . normalize
  in [ {- gstroke 0 G.fractal_sqr -- 1
       , gstroke 0 G.fractal_sqr'
       , gstroke 0 arrows
       , gstroke 0 (flatten arrows)
       , -} f 0 (Graphs.sierpinski (Pt 0 0) 0.5 0.01) -- 5
     , simpleText
     , rectangles
     , semiarcs
     , semiannsS
     , semiannsF -- 10
     , curve_ex
     , arc_ex
     , arcNegative_ex
     , path_ex
     , -- , approx_ex 100 -- 15
       -- , approx_ex 10
       fs_ex
     ]

heps_img :: Image
heps_img = bstroke (Graphs.erat (Pt 48 32) 120)

heps_bb :: BBox
heps_bb = HRBBox 35 19 180 164 40.0 24.0 176.0 160.0

writing :: String -> IO ()
writing fn = print ("writing output to: " ++ fn)

{- | Usage: main [ps_file_path] [eps_file_path]
  Defaults are used for unsupplied args
-}
main :: IO ()
main = do
  a <- getArgs
  u <- getUserDocumentsDirectory
  let ofn = case a of
        x : _ -> x
        _ -> u </> "hps.ps"
      epsfn = case a of
        [_, x] -> x
        _ -> u </> "heps.eps"
  writing ofn
  ps ofn Paper.a4 hps
  writing epsfn
  eps epsfn heps_bb heps_img
