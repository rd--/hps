# hps-csv line

Draw picture of line data stored as (x0,y0,x1,y1) 4-tuples at indicated CSV file.

Parameters are: line-width image-size margin file-stem

The image size is the scalar for the data after it has been normalised into (0,1).

For file-stem of A reads A.csv and writes A.eps, A.pdf and A.svg

~~~~
$ hps-csv line 0.2 400 10 /home/rohan/sw/hps/data/SQ.21.112
$ gv /home/rohan/sw/hps/data/SQ.21.112.eps
$
~~~~
