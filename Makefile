all:
	echo "hps"

mk-cmd:
	(cd cmd ; make all install)

clean:
	rm -f data/*.eps data/*.svg data/*.pdf
	rm -fR dist dist-newstyle
	(cd cmd ; make clean)

push-all:
	r.gitlab-push.sh hps

push-tags:
	r.gitlab-push.sh hps --tags

indent:
	fourmolu -i Graphics cmd

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Graphics
